"""
Vérifie qu'il n'existe pas déja un formulaire soumis par l'utilisateur connecté ayant le meme dpap (numéro de dossier papier)
"""

from wcs.qommon.storage import Equal, NotEqual

# id du champ contenant le DPAP
field_id = [x for x in form_objects.formdef.fields if x.varname == 'dpap'][0].id

noDuplicate = ''
if form.user is None:
    # Si user anonyme on le laisse passer
    noDuplicate = True
else:
    # Si user connecté on check un eventuel doublon : False si doublon, True si pas doublon
    criterias = [Equal('user_id', str(form.user.id)), NotEqual('status', 'draft')]
    noDuplicate = bool(
        len(
            [
                x
                for x in form_objects.formdef.data_class().select(criterias)
                if x.data.get(field_id) == form_var_dpap
            ]
        )
        == 0
    )

result = noDuplicate
